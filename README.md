#### ASE Training Content

### [Day 1](Day_1)
- [Web Development Basics](Day_1)

### [Day 2](Day_2)
- [GIT](Day_2)

### [Day 3](Day_3)
- [HTML](Day_3/HTML)
- [CSS](Day_3/CSS)

### [Day 4](Day_4)
- [Bootstrap 4](Day_4/Bootstrap)
- [Javascript part 1](Day_4/javascript_1)

### [Day 5](Day_5)
- [JavaScript part 2](Day_5)

### [Day 6](Day_6)
- [jQuery](Day_6)

### [Day 7](day_7)
- [XML](Day_7/XML)
- [JSON](Day_7/JSON)
- [XML AJAX](Day_7/XML_AJAX)

### [Day 8](Day_8)
- [SQL part 1](Day_8/SQL_1)

### [Day 9](Day_9)
- [SQL part 2](Day_9)

### [Day 10](Day_10)
- [MYSQL](Day_10)