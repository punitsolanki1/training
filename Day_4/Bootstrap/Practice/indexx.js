$('#name_valid').hide();        
$('#email_valid').hide();       
function check_valid(){
    console.log("hi")
    var name = document.getElementById('name').value;               
    var email = document.getElementById('inputEmail3').value;
    var website = document.getElementById('website').value;
    var image = document.getElementById('image').value;
    var gender = ""
           
    if(document.getElementById('gridRadios1').checked){             
        gender="Male"
    }
    else{
        gender="Female"
    }

    var skills = ""
    if(document.getElementById("gridCheck1").checked){
        skills += "Java"
    }
    if(document.getElementById("gridCheck2").checked){
        if(skills !== "")
            skills += ",HTML"
        else
            skills += "HTML"
    }
    if(document.getElementById("gridCheck3").checked){
        if(skills !== "")
            skills += ",CSS"
        else
            skills += "CSS"
    }
    var valid = true;
    if(name.length<3 || name.length>30){
        valid=false;
        $('#name_valid').show()
    }

    var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if(!reg.test(email)) {
        valid=false;
        $('#email_valid').show()
    }

    if(valid){
        $('#email_valid').hide()
        $('#name_valid').hide()
        document.getElementById('tbody_dec').innerHTML = document.getElementById('tbody_dec').innerHTML + "<tr><th><b>" + name + "</b><br>" + gender + "<br><small>" + email + "<br><a href=\"#\"><u>" + website + "</u></a><br>" + skills + "</small></th><th><img src=\"" + image + "\" class=\"mx-md-4 mx-2\" weight=70px; height=90px;/></th><tr>";
    }
}
function size_change() {                                           
    $("#width").text($(window).width());
    $("#height").text($(window).height());
    if($(this).width() < 989){
        document.getElementById("div1").classList.remove("border-right");           
        document.getElementById("div2").classList.remove("border-left");
        document.getElementById("div1").classList.add("border-bottom");
        document.getElementById("div2").classList.add("border-top");
    }
    else{
        document.getElementById("div1").classList.remove("border-bottom");  
        document.getElementById("div2").classList.remove("border-top");
        document.getElementById("div1").classList.add("border-right");
        document.getElementById("div2").classList.add("border-left");
    }
};
size_change();
$(window).resize(size_change);