$('#name_valid').hide();        
$('#email_valid').hide();       
function check_valid(){
    var name = $('#name').val();               
    var email = $('#inputEmail3').val();
    var website = $('#website').val();
    var image = $('#image').val();
    var gender = ""
           
    if($('#gridRadios1')[0].checked){             
        gender="Male"
    }
    else{
        gender="Female"
    }

    var skills = ""
    if($("#gridCheck1")[0].checked){
        skills += "Java"
    }
    if($("#gridCheck2")[0].checked){
        if(skills !== "")
            skills += ",HTML"
        else
            skills += "HTML"
    }
    if($("#gridCheck3")[0].checked){
        if(skills !== "")
            skills += ",CSS"
        else
            skills += "CSS"
    }
    var valid = true;
    if(name.length<3 || name.length>30){
        valid=false;
        $('#name_valid').show()
    }

    var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if(!reg.test(email)) {
        valid=false;
        $('#email_valid').show()
    }

    if(valid){
        $('#email_valid').hide()
        $('#name_valid').hide()
        $('#tbody_dec').append("<tr><th><b>" + name + "</b><br>" + gender + "<br><small>" + email + "<br><a href=\"#\"><u>" + website + "</u></a><br>" + skills + "</small></th><th><img src=\"" + image + "\" class=\"mx-md-4 mx-2\" weight=70px; height=90px;/></th><tr>");
    }
}
function size_change() {                                           
    $("#width").text($(window).width());
    $("#height").text($(window).height());
    if($(this).width() < 989){
        $("#div1").removeClass("border-right");           
        $("#div2").removeClass("border-left");
        $("#div1").addClass("border-bottom");
        $("#div2").addClass("border-top");
    }
    else{
        $("#div1").removeClass("border-bottom");  
        $("#div2").removeClass("border-top");
        $("#div1").addClass("border-right");
        $("#div2").addClass("border-left");
    }
};
size_change();
$(window).resize(size_change);